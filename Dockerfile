# Stage 1: Compile and Build angular codebase
FROM node:14.15.1 as build
WORKDIR /usr/local/app
COPY ./ /usr/local/app/
RUN npm install
RUN npm run build
RUN ls /usr/local/app/dist

# Stage 2: Serve app with nginx server
FROM nginxinc/nginx-unprivileged
COPY nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=build /usr/local/app/dist/stock-app/ /usr/share/nginx/html
EXPOSE 8080
