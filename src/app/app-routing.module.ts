import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { ChartComponent } from './chart/chart.component';
import { StockCreateComponent } from './stock-create/stock-create.component';
import { StockListComponent } from './stock-list/stock-list.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: 'stock-list' },
  { path: 'create-stock', component: StockCreateComponent },
  { path: 'stock-list', component: StockListComponent },
  { path: 'about', component: AboutComponent},
  { path: 'chart', component: ChartComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
