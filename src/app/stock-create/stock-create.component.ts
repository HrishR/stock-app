import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-stock-create',
  templateUrl: './stock-create.component.html',
  styleUrls: ['./stock-create.component.css']
})
export class StockCreateComponent implements OnInit {

  url:any;

  @Input() stockDetails = {
    id: 0,
    stockTicker: "",
    price: 0,
    buyOrSell: "",
    volume: 0,
    statusCode: 0,
    dateTime: ""    
  }


  constructor(
    public restApi: RestApiService,
    public router: Router
  ) { 
    this.url="http://realtime-stock-data-realtime-stock-data-rtd.punedevopsa52.conygre.com/api/stock/";
  }

  ngOnInit() { }


  moreInfo(stockTicker:any): void {
    if(stockTicker==""){
      alert("Please enter a ticker");
    }
    else {
      window.location.href=this.url+stockTicker;
    }
  }
 

  buyStock() {
    this.stockDetails.buyOrSell = "BUY"
    if(this.stockDetails.stockTicker=="" || this.stockDetails.price==0 || this.stockDetails.volume==0){
        console.log(this.stockDetails);
        alert("Please check if you have entered everything correctly :)");
        return false;
    }
    else {
      console.log(this.stockDetails)
      this.restApi.createStock(this.stockDetails).subscribe((data: {}) => {
        this.router.navigate(['/stock-list'])
      })
      return true;
    }
    
  }

  sellStock() {
    this.stockDetails.buyOrSell = "SELL"
    if(this.stockDetails.stockTicker=="" || this.stockDetails.price==0 || this.stockDetails.volume==0){
        console.log(this.stockDetails);
        alert("Please check if you have entered everything correctly :)");
        return false;
    }
    else {
      console.log(this.stockDetails)
      this.restApi.createStock(this.stockDetails).subscribe((data: {}) => {
        this.router.navigate(['/stock-list'])
      })
      return true;
    }
    
  }


}