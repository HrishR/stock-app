import { Component, OnInit } from '@angular/core';
import { RestApiService } from "../shared/rest-api.service";


@Component({
  selector: 'app-stock-list',
  templateUrl: './stock-list.component.html',
  styleUrls: ['./stock-list.component.css']
})
export class StockListComponent implements OnInit {

  Stocks: any = [];

  dtOptions: DataTables.Settings = {};


  constructor(
    public restApi: RestApiService
  ) { }

  ngOnInit(): void {
    this.loadStocks()   

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 8,
      order: [[5, 'desc']],
      processing: true
     
    };

    setInterval(() => {
      this.loadStocks();      
    }, 10000);
  }

  loadStocks() {
    console.log("hey")
    return this.restApi.getStocks().subscribe((data: {}) => {
      this.Stocks = data;
    })
  }

  deleteStock(id: any) {
    if (window.confirm('Are you sure, you want to delete?')) {
      this.restApi.deleteStock(id).subscribe(data => {
        this.loadStocks()
      })
    }
  }

  reloadCurrentPage() {
    window.location.reload();
  }  

  formatDate(timestamp:string)
  {
    
      var datetime=new Date(timestamp);
      console.log(datetime);
      return datetime;  
  
  }


}
